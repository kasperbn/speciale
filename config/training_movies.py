import sys; sys.path.append('/vagrant/')
from lib.models import *

training_movies = [
  TrainingMovie('/vagrant/data/borgen_error1.mov',     [364,365,366], [1,29,146,186,323,400], frame_offset=5), 
  TrainingMovie('/vagrant/data/angels_share.mov',      [168],         [1,55,420,448],         frame_offset=1),
  TrainingMovie('/vagrant/data/borgen_error.mov',      [462],         [1],                    frame_offset=0),
  TrainingMovie('/vagrant/data/love_last_3_years.mov', [89],          [1,33,98,170,239,285],  frame_offset=1),
  TrainingMovie('/vagrant/data/possession.mov',        [221],         [1,111,231,280],        frame_offset=0),
  TrainingMovie('/vagrant/data/pusher3.mov',           [368,373],     [1,371,462],            frame_offset=1),
  TrainingMovie('/vagrant/data/shadow_dancer.mov',     [208],         [1,8,161,207,257,334],  frame_offset=1)
]