# Ubuntu packages
apt-get update
apt-get install --yes vim postgresql python-dev python-pip libpq-dev python-numpy python-scipy git

# Postgres setup
su postgres -c 'psql -c "create user root;"'
su postgres -c 'psql -c "alter user root superuser;"'
su postgres -c 'psql -c "create database root;"'
psql -c "create user vagrant with encrypted password 'vagrant';"
psql -c "alter user vagrant superuser;"
psql -c "create database vagrant;"
su vagrant -c 'psql -c "create database wise_development;"'  
/etc/init.d/postgresql restart