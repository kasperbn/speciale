#!/usr/bin/env ruby

# vprovision.rb keeps track of your vagrant provisions. 
# It skips installed provisions and only installs new ones.
# 
# Add vprovision.rb and your provision scripts to your app like this:
# your_app/
#   - Vagrantfile
#   - vagrant/
#     - vprovision.rb
#     - provisions/
#       - 01_init_setup.root.sh
#       - 02_init_setup.vagrant.sh
#       - 03_change_postgres_auth.root.sh
#       - 04_change_default_rvm_ruby.vagrant.sh
#
# The format of the script filenames is: 
#
#     "number"_"descriptive_name"."guest_user".sh
#
# number: Use a timestamps or a simple increasing integer. The provisions are run in this order.
# descriptive_name: Give it a meaningful name.
# guest_user: Specify who should run the script on the guest machine (e.g. "root" or "vagrant")
#
# Remember to add vprovision to your Vagrantfile e.g.:
#   Vagrant.configure("2") do |config|
#     config.vm.provision :shell, :path => 'vagrant/vprovision.rb'
#   end

require 'fileutils'

LOG_DIRECTORY = "/home/vagrant/vagrant_provisions"
PROVISIONS_DIRECTORY = "/vagrant/vagrant/provisions"

class Controller

  def run
    create_log_directory
    run_provisions
  end

  def create_log_directory
    FileUtils.mkdir(LOG_DIRECTORY) unless File.directory?(LOG_DIRECTORY)
  end

  def run_provisions
    provisions.each(&:run)
  end
  
  def provisions
    Dir["#{PROVISIONS_DIRECTORY}/*"].map {|f| Provision.new(f) }
  end

end

class Provision
  attr_reader :filename, :user

  def initialize(filepath)
    @filename = filepath.split('/').last
    @user     = filename.split(".")[-2]
  end

  def run
    if has_run?
      puts "Provision: Already provisioned #{filename} - Skipping ..."
    else
      puts "Provision: Provisioning #{filename} as #{user} ..."
      execute_file
      mark_as_run
    end
  end

  def has_run?
    File.exists?("#{LOG_DIRECTORY}/#{filename}")
  end

  def execute_file
    system "su -c 'bash #{PROVISIONS_DIRECTORY}/#{filename}' #{user}"
  end
    
  def mark_as_run 
    FileUtils.touch("#{LOG_DIRECTORY}/#{filename}")
  end
end

Controller.new.run