import sys; sys.path.append('/vagrant/')
from lib import *

name = 'borgen_error1_365'

def to_yuv(rgb):
  r = rgb[:,:,0]
  g = rgb[:,:,1]
  b = rgb[:,:,2]
  
  y =  0.2990*r + 0.5870*g + 0.1140*b
  u = -0.1687*r - 0.3313*g + 0.5000*b + 128
  v =  0.5000*r - 0.4187*g - 0.0813*b + 128
  
  yuv = np.empty_like(rgb)
  yuv[:,:,0] = y
  yuv[:,:,1] = u
  yuv[:,:,2] = v
  
  return yuv

def to_rgb(yuv):
  y = yuv[:,:,0]
  u = yuv[:,:,1]
  v = yuv[:,:,2]

  r = y                   + 1.40200*(v-128)
  g = y - 0.34414*(u-128) - 0.71414*(v-128)
  b = y + 1.77200*(u-128) 

  rgb = np.empty_like(yuv)
  rgb[:,:,0] = r
  rgb[:,:,1] = g
  rgb[:,:,2] = b

  return rgb

a = Image.open('/vagrant/report/graphics/the_data/%s.png'%name)
rgb = np.array(a)

y = to_yuv(rgb)
y[:,:,1] = -128
y[:,:,2] = -128
Image.fromarray(to_rgb(y)).save('/vagrant/report/graphics/color_spaces/%s_y.png'%name)

u = to_yuv(rgb)
# u[:,:,0] = 0
u[:,:,2] = -128
Image.fromarray(to_rgb(u)).save('/vagrant/report/graphics/color_spaces/%s_u.png'%name)

v = to_yuv(rgb)
# v[:,:,0] = 0
v[:,:,1] = -128
Image.fromarray(to_rgb(v)).save('/vagrant/report/graphics/color_spaces/%s_v.png'%name)
