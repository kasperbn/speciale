def Summer(collection):
  sum = 0
  for item in collection:
    sum += item
  return sum

print Summer([1,2,3,4]) # => 10
  
def Printer(message):
  def function(input):
    print message % input  
    return input
  return function

p = Printer('Hello there %s')
p('Alice') # => Hello there Alice
p('Bob')   # => Hello there Bob

class Summer:
  def __call__(self, collection):
    sum = 0
    for item in collection:
      sum += item
    return sum

class Printer:
  def __init__(self, message):
    self.message = message
  def __call__(self, input):
    print self.message % input  
    return input

s = Summer()
print s([1,2,3,4]) # => 10
print s([5,6,7,8]) # => 26

p = Printer('Hello there %s')
p('Alice') # => Hello there Alice
p('Bob')   # => Hello there Bob

class Pipe:
  def __init__(self, *functions):
    self.functions = functions
  def __call__(self, input):
    output = input
    for function in self.functions:
      output = function(output)
    return output

p = Pipe(
  Printer('About to sum the collection %s'),
  Summer(),
  Printer('The sum is %s'),
)
p([1,2,3,4]) 
