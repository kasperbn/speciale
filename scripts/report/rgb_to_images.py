import sys; sys.path.append('/vagrant/')
from lib import *

name = 'borgen_error1_365'

a = Image.open('/vagrant/report/graphics/the_data/%s.png'%name)

red = np.array(a)
red[:,:,1] *=0
red[:,:,2] *=0
Image.fromarray(red).save('/vagrant/report/graphics/color_spaces/%s_red.png'%name)

green = np.array(a)
green[:,:,0] *=0
green[:,:,2] *=0
Image.fromarray(green).save('/vagrant/report/graphics/color_spaces/%s_green.png'%name)

blue = np.array(a)
blue[:,:,0] *=0
blue[:,:,1] *=0
Image.fromarray(blue).save('/vagrant/report/graphics/color_spaces/%s_blue.png'%name)

