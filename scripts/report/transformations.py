import sys; sys.path.append('/vagrant/')
from lib import *

output_dir = '/vagrant/report/graphics/methods/macroblock'
movie      = ProResMovie('/vagrant/data/borgen_error1.mov')
frame      = movie[366]
# movie      = ProResMovie('/vagrant/data/possession.mov')
# frame      = movie[221]
y          = frame.y

# Pipe(MeanBlocks(), PersistImage('%s/mean_blocks.png'%output_dir))(y)
# Pipe(VarianceBlocks(), PersistImage('%s/variance_blocks.png'%output_dir))(y)
# Pipe(AbsDiffToNeighborMeans(), PersistImage('%s/abs_diff_to_neighbor_means.png'%output_dir))(y)
# Pipe(AbsDiffOfAbsDiffToNeighborMeans(), PersistImage('%s/abs_diff_of_abs_diff_to_neighbor_means.png'%output_dir))(y)
# Pipe(SSDiffToNeighborMeans(), PersistImage('%s/ss_diff_to_neighbor_means.png'%output_dir))(y)
# Pipe(AbsDiffToNeighborVariances(), PersistImage('%s/abs_diff_to_neighbor_variances.png'%output_dir))(y)
# Pipe(AbsDiffToNeighborBorders(), PersistImage('%s/abs_diff_to_neighbor_borders.png'%output_dir))(y)
# Pipe(FitNeighbors(), PersistImage('%s/fit_neighbors.png'%output_dir))(y)
# Pipe(FitOnNeighbors(), PersistImage('%s/fit_on_neighbors.png'%output_dir))(y)

output_dir = '/vagrant/report/graphics/methods/straight'

Pipe(PilEdges(), PersistImage('%s/pil_edges.png'%output_dir))(y)
Pipe(AbsDiffOfBlockBorders(), PersistImage('%s/abs_diff_of_block_borders.png'%output_dir))(y)