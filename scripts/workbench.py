import sys; sys.path.append('/vagrant/')
from lib import *

# CalculateAndPrintOutlierResults('mean_blocks', cpus=8)()
# CalculateAndPrintOutlierResults('variance_blocks', cpus=8)()
# CalculateAndPrintOutlierResults('abs_diff_to_neighbor_means', cpus=8)()
# CalculateAndPrintOutlierResults('abs_diff_of_abs_diff_to_neighbor_means', cpus=8)()
# CalculateAndPrintOutlierResults('ss_diff_to_neighbor_means', cpus=8)()
# CalculateAndPrintOutlierResults('abs_diff_to_neighbor_variances', cpus=8)()
# CalculateAndPrintOutlierResults('abs_diff_to_neighbor_borders', cpus=8)()
# CalculateAndPrintOutlierResults('fit_neighbors', cpus=8)()
# CalculateAndPrintOutlierResults('fit_on_neighbors', cpus=8)()

# CalculateAndPrintOutlierResults('pil_edges_>_line_segments(min_length=3)', 'count', cpus=24)()
# CalculateAndPrintOutlierResults('pil_edges_>_border_line_segments(min_length=3)', 'count', cpus=24)()
# CalculateAndPrintOutlierResults('abs_diff_of_block_borders_>_border_line_segments(min_length=3)', 'count', cpus=24)()

t             = training_movies[4]
movie         = ProResMovie(t.movie_path)
plane_name    = 'y'
frame_numbers = range(2, movie.frame_numbers())

title         = '%s %s-plane frames'%(movie.name, plane_name)
xlabel        = 'Frame number'
ylabel        = 'SSIM'

cache_path    = '/vagrant/outputs/experiments/%s/%s/ssim/cached_data.pickle'%(movie.name, plane_name)
time_path     = '/vagrant/outputs/experiments/%s/%s/ssim/time.txt'%(movie.name, plane_name)
plot_path     = '/vagrant/outputs/experiments/%s/%s/ssim/plot.png'%(movie.name, plane_name)

Pipe(
  CachedFunction(cache_path,
    MeasureAndPersistTime(time_path,
      PMap(
        Printer('Mapping frame number %s'),
        GetCurrentAndPrevPlanes(movie, plane_name),
        SSIM
      )
    )
  ),
  Plot(xs=frame_numbers, scene_changes=t.scene_changes, targets=t.targets, frame_offset=t.frame_offset, title=title, ylabel=ylabel, xlabel=xlabel),
  PersistImage(plot_path),
  Printer('Done.')
)(frame_numbers)