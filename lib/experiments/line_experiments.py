from lib.experiments import *
from lib.functions import *
from config.training_movies import *

class LineExperiment(MacroblockExperiment):

  def __call__(self):
    for plotter in [Plot, Plot1stOrderChange]:
      PersistBeforeReduceExperiment(self.movie_path, 
                               self.plane_name, 
                               self.mapper(), 
                               self.reducer(),
                               plotter, 
                               self.frame_offset, 
                               targets=self.targets, 
                               scene_changes=self.scene_changes,
                               frame_start=self.frame_start,
                               frame_end=self.frame_end)()

  def reducer(self):
    return Count()

class LinesInPilEdgesExperiment(LineExperiment):
  def mapper(self):
    return Pipe(PilEdges(), LineSegments(min_segment_length=3))
    
class BlockBorderLinesInPilEdgesExperiment(LineExperiment):
  def mapper(self):
    return Pipe(PilEdges(), BorderLineSegments(min_segment_length=3))
  
class BlockBorderLinesInAbsDiffOfBlockBordersExperiment(LineExperiment):
  def mapper(self):
    return Pipe(AbsDiffOfBlockBorders(), BorderLineSegments(min_segment_length=3))
    