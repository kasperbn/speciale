from calculate_and_print_outlier_results import *
from experiment import *
from map_and_persist_image_experiment import *
from macroblock_experiments import *
from persist_after_reduce_experiment import *
from persist_before_reduce_experiment import *
from line_experiments import *