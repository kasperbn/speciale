from lib.models.pro_res_movie import *
from lib.functions import *

class Experiment:
  
  def __init__(self, movie_path, plane_name, mapper, reducer, plotter, frame_offset, **keywords):
    self.movie_path         = movie_path
    self.plane_name         = plane_name
    self.mapper             = mapper
    self.reducer            = reducer
    self.plotter            = plotter
    self.frame_offset       = frame_offset

    self.movie              = ProResMovie(self.movie_path)

    self.targets            = keywords.pop('targets', [])
    self.scene_changes      = keywords.pop('scene_changes', None)
    self.frame_start        = keywords.pop('frame_start', 1) or 1
    self.frame_end          = keywords.pop('frame_end', self.movie.frame_numbers()) or self.movie.frame_numbers()
    
    self.frame_numbers      = range(self.frame_start, self.frame_end)

    self.movie_name         = self.movie.name
    
    self.mapper_name        = FunctionName()(self.mapper)
    self.mapper_snake_name  = self.mapper_name.replace(" ", "_").lower()    

    self.reducer_name       = FunctionName()(self.reducer)
    self.reducer_snake_name = self.reducer_name.replace(" ", "_").lower()    

    self.plotter_name       = FunctionName()(self.plotter)
    self.plotter_snake_name = self.plotter_name.replace(" ", "_").lower()    

    self.title              = '%s %s-plane frames %s-%s'%(self.movie_name, self.plane_name, self.frame_numbers[0], self.frame_numbers[-1])
    self.xlabel             = 'Frame number'
    self.ylabel             = '%s > %s'%(self.mapper_name, self.reducer_name)

    self.mapper_dir         = '/vagrant/outputs/experiments/%s/%s/%s'%(self.movie_name, self.plane_name, self.mapper_snake_name)
    self.image_dir          = "%s/images"%(self.mapper_dir)
    self.data_path          = "%s/%s-%s/%s.pickle"%(self.mapper_dir, self.frame_numbers[0], self.frame_numbers[-1], self.reducer_snake_name)
    self.frame_data_dir     = "%s/data"%(self.mapper_dir)
    self.plot_path          = "%s/%s-%s/%s/%s.png"%(self.mapper_dir, self.frame_numbers[0], self.frame_numbers[-1], self.reducer_snake_name,self.plotter_snake_name)
    self.time_data_path     = "%s/%s-%s/%s/%s_time.txt"%(self.mapper_dir, self.frame_numbers[0], self.frame_numbers[-1], self.reducer_snake_name, self.plotter_snake_name)

    self.experiment_name    = '%s-%s[%s:%s] > %s > %s > %s'%(self.movie_name, self.plane_name, self.frame_numbers[0], self.frame_numbers[-1], self.mapper_name, self.reducer_name, self.plotter_name)

  def __call__(self):
    self.scene_changes = self.scene_changes or DetectScenes()(self.movie)
    title='Running %s with %s'%(self.__class__.__name__, self.experiment_name)
    print title
    print '='*len(title)
    self.run()
    print ''
  
  def run(self):
    raise NotImplemented