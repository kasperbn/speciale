from experiment import *
from lib.functions import *

class PersistAfterReduceExperiment(Experiment):

  def run(self):
    Pipe(
      Measure(
        CachedFunction(
          self.data_path,
          PMap(
            Printer('Mapping frame number %s'),
            GetPlane(self.movie, self.plane_name),
            self.mapper,
            PersistPlaneImage(self.image_dir),
            self.reducer
          ),
        ),
        self.plotter(xs=self.frame_numbers, scene_changes=self.scene_changes, targets=self.targets, frame_offset=self.frame_offset, title=self.title, ylabel=self.ylabel, xlabel=self.xlabel),
        PersistImage(self.plot_path)
      ),
      PersistData(self.time_data_path),
      Printer('Time in seconds: %s'),
      Printer('Successfully saved mapped images to: %s'%(self.image_dir)),
      Printer('Successfully saved data to: %s'%(self.data_path)),
      Printer('Successfully saved plot to: %s'%(self.plot_path))
    )(self.frame_numbers)