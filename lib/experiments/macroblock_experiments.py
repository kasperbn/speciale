from lib.experiments.persist_after_reduce_experiment import *
from lib.experiments.map_and_persist_image_experiment import *
from lib.functions import *
from config.training_movies import *

class MacroblockExperiment:
  
  def __init__(self, movie_path, plane_name, frame_start, frame_end):
    self.movie_path    = movie_path
    self.plane_name    = plane_name
    self.frame_start   = frame_start
    self.frame_end   = frame_end    

    self.frame_offset  = 0
    self.targets       = []
    self.scene_changes = []
    
    self.name          = self.__class__.__name__

    # Find training_movie
    for t in training_movies:
      if t.movie_path == self.movie_path:
        self.frame_offset   = t.frame_offset
        self.targets        = t.targets
        self.scene_changes  = t.scene_changes
        break

  def __call__(self):
    for plotter in [Plot, Plot1stOrderChange]:
      PersistAfterReduceExperiment(self.movie_path, 
                               self.plane_name, 
                               self.mapper(), 
                               self.reducer(),
                               plotter, 
                               self.frame_offset, 
                               targets=self.targets, 
                               scene_changes=self.scene_changes,
                               frame_start=self.frame_start,
                               frame_end=self.frame_end)()

  def mapper(self):
    raise NotImplemented

  def reducer(self):
    return Pipe(BlockMeans(), Sum())

class AbsDiffToNeighborMeansExperiment(MacroblockExperiment):
  def mapper(self):
    return AbsDiffToNeighborMeans()

class AbsDiffToNeighborBordersExperiment(MacroblockExperiment):
  def mapper(self):
    return AbsDiffToNeighborBorders()

class AbsDiffOfAbsDiffToNeighborMeansExperiment(MacroblockExperiment):
  def mapper(self):
    return AbsDiffOfAbsDiffToNeighborMeans()

class AbsDiffToNeighborVariancesExperiment(MacroblockExperiment):
  def mapper(self):
    return AbsDiffToNeighborVariances()

class FitNeighborsExperiment(MacroblockExperiment):
  def mapper(self):
    return FitNeighbors()

class FitOnNeighborsExperiment(MacroblockExperiment):
  def mapper(self):
    return FitOnNeighbors()

class MeanBlocksExperiment(MacroblockExperiment):
  def mapper(self):
    return MeanBlocks()

class SSDiffToNeighborMeansExperiment(MacroblockExperiment):
  def mapper(self):
    return SSDiffToNeighborMeans()

class VarianceBlocksExperiment(MacroblockExperiment):
  def mapper(self):
    return VarianceBlocks()