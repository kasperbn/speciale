from lib.functions import *
from config import *
import os
import string

class CalculateAndPrintOutlierResults:
  def __init__(self, experiment_name, reducer_name='block_means_>_sum', cpus=8):
    self.experiment_name = experiment_name
    self.reducer_name    = reducer_name
    self.cpus            = cpus
  def __call__(self, start_q_factor=10):
    Pipe(
      Printer('Finding outliers in %s'%self.experiment_name),
      SimulatedAnnealing(q_factor_proposal, q_factor_cost(self.experiment_name, self.reducer_name, self.cpus), q_factor_temperature, 20),
      Printer("\caption{Outlier Detection in %s, Q-factor = %%s}"%self.experiment_name),
      OutlierResults(self.experiment_name, self.reducer_name, self.cpus),
      PrintLatex(self.experiment_name),
      # PrintPretty(self.experiment_name),
    )(start_q_factor)

def q_factor_temperature(iteration):
  return 1+100*0.9**(iteration-1)

def q_factor_proposal(q_factor):
  return q_factor + np.random.normal(0,5,1)[0]

def q_factor_cost(experiment_name, reducer_name, cpus):

  def call(q_factor):
    plane_results, total_results = OutlierResults(experiment_name, reducer_name, cpus)(q_factor)
    return Sum()([r.cost for p,r in total_results.iteritems()])

  return call

class OutlierResults:
  def __init__(self, experiment_name, reducer_name, cpus):
    self.experiment_name = experiment_name
    self.reducer_name    = reducer_name
    self.cpus            = cpus
  def __call__(self, q_factor):
    plane_results = {
      'y': self.plane_outlier_results('y',q_factor),
      'u': self.plane_outlier_results('u',q_factor),
      'v': self.plane_outlier_results('v',q_factor)
    }
    total_results = {
      'y': TotalResult(plane_results['y']),
      'u': TotalResult(plane_results['u']),
      'v': TotalResult(plane_results['v'])
    }
    return (plane_results, total_results)

  def plane_outlier_results(self, plane_name, q_factor):    
    results = {}
    for t in training_movies:
      try:
        movie               = ProResMovie(t.movie_path)    
        data, time          = LoadMovieData(self.experiment_name, self.reducer_name, movie.name, plane_name, self.cpus)()
        results[movie.name] = MovieOutlierResult(q_factor, t.scene_changes, t.targets, time)(data)
      except:
        print 'No data for %s %s'%(t.movie_path, plane_name)
    return results

class MovieOutlierResult:
  def __init__(self, q_factor, scene_changes, targets, time):
    self.q_factor      = q_factor
    self.scene_changes = scene_changes
    self.targets       = targets
    self.time          = time
  def __call__(self, data):
    outlier_frames = Pipe(
      SceneRanges(self.scene_changes),
      Map(Outliers(self.q_factor)),
      Flatten(),
      Indices(data)
    )(data)
    return OutlierResult(self.targets, outlier_frames, len(data), self.time)

class Indices:
  def __init__(self, data):
    self.data = data
  def __call__(self, values):
    return [i for i in range(0,len(self.data)) if self.data[i] in values]

class LoadMovieData:
  def __init__(self, experiment_name, reducer_name, movie_name, plane_name, cpus):
    self.experiment_name = experiment_name
    self.reducer_name    = reducer_name
    self.movie_name      = movie_name
    self.plane_name      = plane_name
    self.cpus            = cpus
  def __call__(self):
    for frame_range in os.listdir('/vagrant/outputs/experiments/%s/%s/%s'%(self.movie_name,self.plane_name,self.experiment_name)):
      if frame_range != '.DS_Store' and frame_range != 'images':
        data_path = '/vagrant/outputs/experiments/%s/%s/%s/%s/%s.pickle'%(self.movie_name,self.plane_name,self.experiment_name,frame_range, self.reducer_name)
        time_path = '/vagrant/outputs/experiments/%s/%s/%s/%s/%s/plot_time.txt'%(self.movie_name,self.plane_name,self.experiment_name,frame_range, self.reducer_name)
        data      = Pipe(LoadData(), FirstOrderChanges())(data_path)
        time      = LoadData()(time_path) * self.cpus / float(len(data))
        return data, time
  
class PrintLatex:
  def __init__(self, experiment_name):
    self.experiment_name = experiment_name
  def __call__(self, results):
    plane_results, total_results = results
    print "\\begin{figure}[H]"
    print "\t\centering"
    print "\t\\begin{tabular}{|l|r|r|r|r|r|r|}"
    for plane_name,movie_names in plane_results.iteritems():
      print "\t\t\hline"
      print "\t\t%s-plane              & \multicolumn{2}{c|}{True Positives} & \multicolumn{2}{c|}{False Positives} & Time per frame & Cost\\\\"%plane_name
      print "\t\t\hline"
      for movie_name, r in movie_names.iteritems():
        print "\t%s & %s/%s & %s\%% & %s/%s & %s\%% & %.2fs & %s \\\\"%(string.ljust(movie_name.replace('_','\\_'),20), r.true_positives, r.num_targets, r.true_positives_percentage, r.false_positives, r.num_non_targets, r.false_positives_percentage, r.time, r.cost)
    print "\t\t\hline"
    for plane_name, r in total_results.iteritems():
      print "\t\t%s Total              & %s/%s & %s\%% & %s/%s & %3s\%% & (mean) %.2fs & %s \\\\"%(plane_name.upper(), r.true_positives, r.num_targets, r.true_positives_percentage, r.false_positives, r.num_non_targets, r.false_positives_percentage, r.time, r.cost)
    print "\t\t\hline"
    print "\t\\end{tabular}"
    print "\\end{figure}"
    print "no. of cpu core required: %s"%(total_results['y'].time*(25*60*60*2)/(60*60*8))

class PrintPretty:
  def __init__(self,experiment_name):
    self.experiment_name = experiment_name  
  def __call__(self, results):
    plane_results, total_results = results
    print "========================================================"
    print "|%s|"%string.center(self.experiment_name.upper(), 54)
  
    for plane_name,movie_names in plane_results.iteritems():
      print "--------------------------------------------------------"
      print "| %s-plane           | True Positives | False Positives |"%plane_name
      print "--------------------------------------------------------"
      for movie_name, r in movie_names.iteritems():
        print "| %17s |   %1s/%1s (%3s%%)   |  %3s/%3s (%3s%%) |"%(movie_name, r.true_positives, r.num_targets, r.true_positives_percentage, r.false_positives, r.num_non_targets, r.false_positives_percentage)
  
    print "--------------------------------------------------------"
    for plane_name, r in total_results.iteritems():
      print "|%s Total              | %s/%s (%s%%) | %s/%s | %3s%% | "%(plane_name.upper(), r.true_positives, r.num_targets, r.true_positives_percentage, r.false_positives, r.num_non_targets, r.false_positives_percentage)
    print "--------------------------------------------------------"

class Result:
  def __init__(self, true_positives, false_positives, num_targets, num_non_targets, time):
    self.true_positives  = true_positives
    self.false_positives = false_positives
    self.num_targets     = num_targets
    self.num_non_targets = num_non_targets
    self.time            = time

    self.num_total       = self.num_targets + self.num_non_targets
    self.true_negatives  = self.num_non_targets - self.false_positives
    self.false_negatives = self.num_targets - self.true_positives
    
    self.cost_weights    = np.array([0,1,0,100])
    self.cost            = Sum()([self.true_positives, self.false_positives, self.true_negatives, self.false_negatives] * np.array([0,1,0,100]))

    self.true_positives_percentage  = int(100*self.true_positives / float(self.num_targets))
    self.false_positives_percentage = int(100*self.false_positives / float(self.num_non_targets))

def OutlierResult(targets, outliers, num_total, time):
  true_positives             = len([x for x in targets if x in outliers])
  false_positives            = len([x for x in outliers if x not in targets])
  num_targets                = len(targets)
  num_non_targets            = num_total - len(targets)
  return Result(true_positives, false_positives, num_targets, num_non_targets, time)
    
def TotalResult(results):
  true_positives  = Sum()([r.true_positives  for m,r in results.iteritems()])
  false_positives = Sum()([r.false_positives for m,r in results.iteritems()])
  num_targets     = Sum()([r.num_targets     for m,r in results.iteritems()])
  num_non_targets = Sum()([r.num_non_targets for m,r in results.iteritems()])
  time            = np.array(([r.time for m,r in results.iteritems()])).mean()
  return Result(true_positives, false_positives, num_targets, num_non_targets, time)