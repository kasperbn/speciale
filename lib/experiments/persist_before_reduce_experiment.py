from experiment import *

class PersistBeforeReduceExperiment(Experiment):
 
  def run(self):
    self.mapper_data_path = "%s/%s-%s.pickle"%(self.mapper_dir, self.frame_numbers[0], self.frame_numbers[-1])
    Pipe(
      Measure(
        CachedFunction(self.data_path,
          Pipe(
            CachedFunction(self.mapper_data_path,
              PMap(
                Printer('Mapping frame number %s'),
                GetPlane(self.movie, self.plane_name),
                self.mapper,
              ),
            ),
            PMap(self.reducer),
          )
        ),
        self.plotter(xs=self.frame_numbers, scene_changes=self.scene_changes, targets=self.targets, frame_offset=self.frame_offset, title=self.title, ylabel=self.ylabel, xlabel=self.xlabel),
        PersistImage(self.plot_path)
      ),
      PersistData(self.time_data_path),
      Printer('Time in seconds: %s'),
      Printer('Successfully saved data to: %s'%(self.data_path)),
      Printer('Successfully saved plot to: %s'%(self.plot_path))
    )(self.frame_numbers)
