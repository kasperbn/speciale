class FirstOrderChanges:
  def __call__(self,ys):
    last_y  = ys[1]
    changes = []
    for y in ys:
      change = y-last_y
      last_y = y
      changes.append(change)
    return changes