import numpy as np

class Outliers:
  
  def __init__(self, factor=1.5):
    self.factor = factor

  def __call__(self, collection):
    col         = sorted(collection)
    half_width  = len(col)/2
    lower,upper = col[:half_width],col[half_width:]
    q1          = np.median(lower)
    q3          = np.median(upper)
    iqr         = q3 - q1
    outliers    = [x for x in col if x < q1 - self.factor*iqr or x > q3 + self.factor*iqr]
    return outliers

class OutlierIndices(Outliers):
  def __call__(self, collection):
    outliers = Outliers(self.factor)(collection)
    indices  = [i for i in range(0,len(collection)) if collection[i] in outliers]
    return indices