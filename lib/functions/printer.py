class Printer:
  
  def __init__(self, string):
    self.string = string
  
  def __call__(self, _object):
    try:
      print self.string%_object
    except:
      print self.string
    return _object