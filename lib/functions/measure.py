import time
from lib.functions.pipe import *
from lib.functions.persist_data import *

class Measure:
  
  def __init__(self, *functions):
    self.function = Pipe(*functions)

  def __call__(self, *args):
    _t0  = time.time()
    self.function(*args)
    _t1  = time.time()
    return _t1 - _t0

class MeasureAndPersistTime:
  
  def __init__(self, path, *functions):
    self.path = path
    self.function = Pipe(*functions)
  
  def __call__(self, *args):
    _t0   = time.time()
    value = self.function(*args)
    _t1   = time.time()
    PersistData(self.path)(_t1-_t0)
    return value