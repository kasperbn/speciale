from lib.models.coordinate import *

class BlockNeighbors:
  
  def __init__(self, block):
    self.block      = block

    self.plane      = self.block.block_plane
    self.coordinate = self.block.coordinate()

  def __call__(self):
    return self.coordinates_to_blocks(self.coordinates())
      
  def coordinates(self):
    raise NotImplemented
        
  def on_north_edge(self):
    return self.coordinate.y == 0
    
  def on_west_edge(self):
    return self.coordinate.x == 0
    
  def on_east_edge(self):
    return self.coordinate.x == self.plane.width() - 1
    
  def on_south_edge(self):
    return self.coordinate.y == self.plane.height() - 1

  def coordinates_to_blocks(self, coordinates):
    neighbors = []
    for coordinate in coordinates:
      neighbors.append(self.plane[coordinate])
    return neighbors

class BlockEightNeighbors(BlockNeighbors):

  def coordinates(self):
    x,y = self.coordinate.tuple()
    neighbors = []
    if not self.on_north_edge() and not self.on_west_edge():
      neighbors.append(Coordinate(x-1,y-1))
    if not self.on_north_edge():
      neighbors.append(Coordinate(x  ,y-1))
    if not self.on_north_edge() and not self.on_east_edge():
      neighbors.append(Coordinate(x+1,y-1))
    if not self.on_west_edge():
      neighbors.append(Coordinate(x-1,y  )) 
    if not self.on_east_edge():
      neighbors.append(Coordinate(x+1,y  ))
    if not self.on_south_edge() and not self.on_west_edge():
      neighbors.append(Coordinate(x-1,y+1))
    if not self.on_south_edge():
      neighbors.append(Coordinate(x  ,y+1))
    if not self.on_south_edge() and not self.on_east_edge():
      neighbors.append(Coordinate(x+1,y+1))
    return neighbors

class BlockFourNeighbors(BlockNeighbors):

  def coordinates(self):
    x,y = self.coordinate.tuple()
    neighbors = []
    if not self.on_north_edge():
      neighbors.append(Coordinate(x  ,y-1))
    if not self.on_west_edge():
      neighbors.append(Coordinate(x-1,y  ))
    if not self.on_east_edge():
      neighbors.append(Coordinate(x+1,y  ))
    if not self.on_south_edge():
      neighbors.append(Coordinate(x  ,y+1))
    return neighbors