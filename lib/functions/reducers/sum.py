class Sum:
  def __call__(self, collection):
    return sum(collection)
  