class BlockMeans:
  def __call__(self, block_plane):
    return [b.mean() for b in block_plane]