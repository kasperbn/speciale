from persist_data import *
from pipe import *

class CachedFunction:
  
  def __init__(self, persist_path, *functions):
    self.persist_path = persist_path
    self.function     = Pipe(*functions)

  def __call__(self, _object):
    try:
      return LoadData()(self.persist_path)
    except:
      return PersistData(self.persist_path)(self.function(_object))