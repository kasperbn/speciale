from lib.functions.flatten import *

class SceneRanges:
  def __init__(self, scene_changes, xs=None):
    self.scene_changes = scene_changes
    self.xs            = xs
  def __call__(self, data):
    if not self.xs:
      self.xs = range(1,len(data))
      
    present_scene_changes = [s for s in self.scene_changes if s in self.xs]
    if not self.xs[0]  in present_scene_changes:
      present_scene_changes.insert(0, self.xs[0]) 
    if not self.xs[-1]+1 in present_scene_changes:
      present_scene_changes.append(self.xs[-1]+1)

    scenes = []
    scene_start = present_scene_changes[0]
    for next_scene_start in present_scene_changes[1:]:
      scene_end   = next_scene_start-1
      start_index = self.xs.index(scene_start)
      end_index   = self.xs.index(scene_end)+1
      scenes.append(data[start_index:end_index])
      scene_start = next_scene_start

    return scenes