import numpy as np

class SimulatedAnnealing:

  def __init__(self, proposal_function, cost_function, temperature_function, max_iterations):
    self.proposal_function    = proposal_function
    self.cost_function        = cost_function
    self.temperature_function = temperature_function
    self.max_iterations       = max_iterations
  
  def __call__(self, parameters):
    cost       = 1000000
    for iteration in range(1,self.max_iterations):
      temperature    = self.temperature_function(iteration)
      candidate      = self.proposal_function(parameters)
      candidate_cost = self.cost_function(candidate)

      if candidate_cost < cost:
        parameters = candidate
        cost       = candidate_cost
      else:
        p = np.exp(-((candidate_cost - cost) / temperature))
        if np.random.uniform(0,1,1) <= p:
          parameters = candidate
          cost       = candidate_cost

      print 'Iteration %s, Temperature %s, Parameters %s, Costs %s'%(iteration, temperature, parameters, cost)

    return parameters