from PIL import Image, ImageFilter
from math import hypot, pi, cos, sin
import Image
import matplotlib
matplotlib.use('Agg')
import matplotlib.pyplot as plt
import numpy as np

class Plot:

  def __init__(self, **keywords):    
    self.xs            = keywords.pop('xs', None)
    self.targets       = keywords.pop('targets', [])
    self.scene_changes = keywords.pop('scene_changes', None)
    self.frame_offset  = keywords.pop('frame_offset', 0)
    self.title         = keywords.pop('title', "")
    self.xlabel        = keywords.pop('xlabel', "")
    self.ylabel        = keywords.pop('ylabel', "")

  def __call__(self, ys):    
    xs              = self.xs or range(1,len(ys)+1)
    [xs,ys,targets] = self.offset_data(self.frame_offset, xs, ys, self.targets)
    return self.draw_plot(xs,ys,targets)
  
  def draw_plot(self, xs, ys, targets):
    fig             = plt.figure()

    present_scene_changes = [s for s in self.scene_changes if s in xs]
    if not xs[0]  in present_scene_changes:
      present_scene_changes.insert(0, xs[0]) 
    
    if not xs[-1]+1 in present_scene_changes:
      present_scene_changes.append(xs[-1]+1)

    self.plot_target_values(targets, xs, ys)
    self.plot_scenes(present_scene_changes, xs, ys)
    self.plot_vertical_scene_changes(present_scene_changes)
    
    plt.title(self.title)
    plt.xlabel(self.xlabel)
    plt.ylabel(self.ylabel)
    plt.xlim(xs[0],xs[-1])

    return plt

  def offset_data(self, frame_offset, xs, ys, targets):
    if frame_offset > 0:
      xs      = xs[:-frame_offset]
      ys      = ys[frame_offset:]
      targets = [t - frame_offset for t in targets if t <= xs[-1]]
    return [xs,ys,targets]

  def plot_target_values(self, targets, xs, ys):
    for target in targets:
      try:
        tx = xs.index(target)
        plt.plot(target, ys[tx], 'rD')
      except:
        pass # The target values is not in range

  def plot_scenes(self, scene_changes, xs, ys):        
    scene_start = scene_changes[0]
    for next_scene_start in scene_changes[1:]:
      scene_end   = next_scene_start-1
      start_index = xs.index(scene_start)
      end_index   = xs.index(scene_end)+1
      scene_xs    = xs[start_index:end_index]
      scene_ys    = ys[start_index:end_index]
      self.plot_scene(scene_start, scene_end, scene_xs, scene_ys)
      scene_start = next_scene_start

  def plot_vertical_scene_changes(self, scene_changes):
    for scene in scene_changes:
      plt.axvline(x=scene, color='y', linestyle='-')
  
  def plot_scene(self, scene_start, scene_end, scene_xs, scene_ys):
    mean              = np.array(scene_ys).mean()
    std               = np.array(scene_ys).std()

    # x and y
    plt.plot(scene_xs, scene_ys)

    # mean
    plt.plot([scene_start, scene_end], [mean,mean], color='y', linestyle='--')
    
    # upper std
    plt.plot([scene_start, scene_end], [mean+std,mean+std], color='y', linestyle='-.')
    
    # lower std
    plt.plot([scene_start, scene_end], [mean-std,mean-std], color='y', linestyle='-.')