from lib.functions.plots.plot import *
from lib.functions.pipe import *
from lib.functions.scene_ranges import *
from lib.functions.map import *
from lib.functions.first_order_changes import *
from lib.functions.flatten import *

class Plot1stOrderChange(Plot):
    
  def __call__(self, ys):
    xs              = self.xs or range(1,len(ys)+1)
    [xs,ys,targets] = self.offset_data(self.frame_offset, xs, ys, self.targets)

    self.ylabel = "Change in %s" % self.ylabel
    ys = Pipe(
          SceneRanges(self.scene_changes, xs),
          Map(FirstOrderChanges()),
          Flatten()
        )(ys)

    return self.draw_plot(xs,ys,targets)