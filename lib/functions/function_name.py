import re
import inspect

class FunctionName:
  
  def __call__(self, function):
    if hasattr(function, 'name'):
      return function.name()
    else:      
      return self.camelcase_to_human(self.camelcase(function))
    
  def camelcase(self, function):
    if inspect.isclass(function):
      return function.__name__
    else:
      return function.__class__.__name__

  def camelcase_to_human(self, name):
      s1 = re.sub('(.)([A-Z][a-z]+)', r'\1 \2', name)
      return re.sub('([a-z0-9])([A-Z])', r'\1 \2', s1)