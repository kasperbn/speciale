from function_name import *

class Pipe:
  
  def __init__(self, *functions):
    self.functions = functions
  
  def __call__(self, _input):
    for function in self.functions:
      _input = function(_input)
    return _input

  def name(self):
    return ' > '.join([FunctionName()(m) for m in self.functions])