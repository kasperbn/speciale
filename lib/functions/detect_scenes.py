import os

class DetectScenes:

  def __init__(self, **keywords):
    self.detection_threshold = keywords.pop('detection_threshold',0.15)
    
  def __call__(self, movie):
    print "Detecting scenes in %s"%(movie.name)
    frames = [1]
    shellquoted_file_path = movie.file_path.replace(" ", "\ ")
    command = '/home/vagrant/bin/ffprobe -v quiet -show_frames -select_streams v -f lavfi "movie=%s,select=gt(scene\,%s)" | grep "pkt_pts="' % (shellquoted_file_path, self.detection_threshold)
    raw = os.popen(command).read()
    arr = raw.split("\n")
    for frame in arr:
      try:
        start_frame = frame.split('=')[1]
        start_frame = int(start_frame)
        frames.append(start_frame * movie.frame_rate / movie.time_base)
      except:
        pass # Extra newline
    return frames