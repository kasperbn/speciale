from scipy import misc
import os

from lib.models.block_plane import *
from lib.models.pixel_plane import *

class PersistImage:

  def __init__(self, path):
    self.path = path

  def __call__(self, plane_or_plot):
    klass = plane_or_plot.__class__
    if (klass == BlockPlane or klass == np.array):
      return PersistPlane(self.path)(plane_or_plot)
    else:
      return PersistPlot(self.path)(plane_or_plot)
  
class PersistPlane(PersistImage):
  def __call__(self, block_plane):
    CreateDirUnlessExists()(self.path)
    array = block_plane.pixels
    cmax  = max([255, array.max()])
    image = misc.toimage(array, cmin=0, cmax=cmax)
    image.save(self.path)
    return block_plane

class PersistPlot(PersistImage):
  def __call__(self, plot):
    CreateDirUnlessExists()(self.path)
    plot.savefig(self.path)
    return plot
  
class CreateDirUnlessExists:
  def __call__(self, path):
    directory = '/'.join(path.split('/')[:-1])
    if not os.path.exists(directory):
      os.makedirs(directory)
    return path