class GetPlane:
  
  def __init__(self, movie, plane_name):
    self.movie      = movie
    self.plane_name = plane_name
  
  def __call__(self, frame_number):
    return getattr(self.movie[frame_number], self.plane_name)
  
  def name(self):
    return "%s_%s"%(self.movie.name, self.plane_name)

class GetCurrentAndPrevPlanes(GetPlane):
  
  def __call__(self, frame_number):
    c = getattr(self.movie[frame_number], self.plane_name)
    p = getattr(self.movie[frame_number-1], self.plane_name)
    return [c, p]