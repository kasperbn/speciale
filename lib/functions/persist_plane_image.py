from persist_image import PersistPlane

class PersistPlaneImage:
  def __init__(self, image_dir):
    self.image_dir = image_dir

  def __call__(self, block_plane):  
    image_path = "%s/%s.png"%(self.image_dir, block_plane.frame_number)
    PersistPlane(image_path)(block_plane)
    return block_plane