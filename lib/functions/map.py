import logging
from pipe import *
from multiprocessing import Pool

class Map(Pipe):
  def __init__(self, *functions):
    self.function  = Pipe(*functions)
  
  def __call__(self, collection):
    return [self.function(e) for e in collection]

class PMap(Map):
  def __init__(self, *functions):
    self.function = Pipe(*functions)
    self.pool     = Pool()

  def __call__(self, collection):
    r = self.pool.map(self.function, collection)
    self.pool.close()
    return r