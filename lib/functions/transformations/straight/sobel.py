import numpy as np
from scipy import ndimage
from PIL import Image
import scipy

class Sobel:
  
  def __call__(self, block_plane):
    transformed = block_plane.dup()

    image       = Image.fromarray(transformed.pixels)
    dx          = ndimage.sobel(image, 0)  # horizontal derivative
    dy          = ndimage.sobel(image, 1)  # vertical derivative
    magnitude   = np.hypot(dx, dy)  
    magnitude  *= 255.0 / np.max(magnitude)  # normalize (Q&D)

    transformed.pixels = np.array(magnitude)

    return transformed