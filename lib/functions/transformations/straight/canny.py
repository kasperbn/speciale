import numpy as np
from scipy import ndimage
from PIL import Image
from skimage import filter

class Canny:
  
  def __call__(self, block_plane):
    new_block_plane = block_plane.dup()

    image = Image.fromarray(new_block_plane.pixels)
    image = np.array(image)
    image = ndimage.gaussian_filter(image, 1)
    image = filter.canny(image, sigma=1)
    image = ndimage.binary_opening(image).astype(np.float) * 255

    new_block_plane.pixels = image
    return new_block_plane