# from lib.functions.persist_data import *
# from lib.functions.persist_image import *
# import otbApplication 
# import os
# import ogr
# from scipy import misc
# import cv2
# import math
# 
# class LSD:
# 
#   def __call__(self, block_plane):  
#     input_path  = '/vagrant/tmp/line_detection_input.png'
#     shp_path    = '/vagrant/tmp/line_detection_output.shp'
#     output_path = '/vagrant/tmp/line_detection_output.png'    
#     PersistImage(input_path)(block_plane)
#     
#     #  
#     # LineSegmentDetection = otbApplication.Registry.CreateApplication("LineSegmentDetection") 
#     # LineSegmentDetection.SetParameterString("in", input_path) 
#     # LineSegmentDetection.SetParameterString("out", shp_path) 
#     # LineSegmentDetection.ExecuteAndWriteOutput()
#     # dataset = ogr.Open(shp_path)
#     # return dataset.
#     
#     cv2.lsd
#     
#     img   = cv2.imread(input_path)
#     gray  = cv2.cvtColor(img, cv2.COLOR_BGR2GRAY)
#     edges = cv2.Canny(gray, 80, 120)
#     lines = cv2.HoughLinesP(edges, 1, math.pi/2, 2, None, 30, 1);
#     for line in lines[0]:
#         pt1 = (line[0],line[1])
#         pt2 = (line[2],line[3])
#         cv2.line(img, pt1, pt2, (0,0,255), 3)
#     cv2.imwrite(output_path, img)
#     
#     new_block_plane        = block_plane.dup()
#     new_block_plane.pixels = misc.imread(output_path)
#     return new_block_plane