from lib.models.line_segment import *
from lib.models.block_plane import *
from line_segments import *

class BorderLineSegments(LineSegments):
  
  def __call__(self, block_plane):
    h = HorizontalBorderLineSegments(self.min_segment_length)(block_plane)
    v = VerticalBorderLineSegments(self.min_segment_length)(block_plane)
    return h + v

class VerticalBorderLineSegments(LineSegments):

  def __call__(self, block_plane):
    rotated = block_plane.rot90()
    return HorizontalBorderLineSegments(self.min_segment_length)(rotated)

class HorizontalBorderLineSegments(HorizontalLineSegments):

  def __call__(self, block_plane):
    pixels             = block_plane.pixels
    min_segment_length = self.min_segment_length or block_plane.block_width
    block_height       = block_plane.block_height
    block_width        = block_plane.block_width
    width              = block_plane.pixel_plane_width
    height             = block_plane.pixel_plane_height

    extra_col          = 0 if width  % block_width  == 0 else 1
    extra_row          = 0 if height % block_height == 0 else 1

    tops               = [y_block * block_height     for y_block in range(0,block_plane.height())]
    bottoms            = [y_block * block_height - 1 for y_block in range(1,block_plane.height()+1)]
    rows               = tops + bottoms

    cols               = range(0,width)

    return self.count(pixels, min_segment_length, rows, cols)