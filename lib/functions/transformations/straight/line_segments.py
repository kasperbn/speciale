from lib.models.line_segment import *
from lib.models.block_plane import *
from lib.functions.function_name import *

class LineSegments:

  def __init__(self, min_segment_length=None):
    self.min_segment_length = min_segment_length
  
  def __call__(self, block_plane):
    h = HorizontalLineSegments(self.min_segment_length)(block_plane)
    v = VerticalLineSegments(self.min_segment_length)(block_plane)
    return h + v

  def name(self):
    return '%s(min_length=%s)'%(self.camelcase_to_human(self.__class__.__name__), self.min_segment_length)

  def camelcase_to_human(self, name):
      s1 = re.sub('(.)([A-Z][a-z]+)', r'\1 \2', name)
      return re.sub('([a-z0-9])([A-Z])', r'\1 \2', s1)

class VerticalLineSegments(LineSegments):

  def __call__(self, block_plane):
    rotated = block_plane.rot90()
    return HorizontalLineSegments(self.min_segment_length)(rotated)

class HorizontalLineSegments(LineSegments):  

  def __call__(self, block_plane):
    pixels             = block_plane.pixels
    min_segment_length = self.min_segment_length or block_plane.block_width
    width              = block_plane.pixel_plane_width
    height             = block_plane.pixel_plane_height

    rows               = range(0,height)
    cols               = range(0,width)

    return self.count(pixels, min_segment_length, rows, cols)

  def count(self, pixels, min_segment_length, rows, cols):
    median        = (pixels.max() - pixels.min()) / 2
    line_segments = []
    current       = []
    
    for y in rows:
      for x in cols:
        pixel = pixels[y,x]
        if pixel >= median:
          current.append(pixel)
        else:
          if len(current) >= min_segment_length:
            line_segments.append(current)
          current = []
    
    return [LineSegment(s) for s in line_segments]