import numpy as np
from lib.models.block_plane import *

class AbsDiffOfBlockBorders:
  def __call__(self, block_plane):
    h = AbsDiffOfHorizontalBlockBorders()(block_plane)
    v = AbsDiffOfVerticalBlockBorders()(block_plane)
    d = block_plane.dup()
    d.pixels = h.pixels + v.pixels
    return d

class AbsDiffOfVerticalBlockBorders:
  def __call__(self, block_plane):
    rotated = block_plane.rot90()
    return AbsDiffOfHorizontalBlockBorders()(rotated).rot90().rot90().rot90()

class AbsDiffOfHorizontalBlockBorders:

  # Calculate the absolute difference of the horizontal block borders
  #
  # Returns a block plane with the following structure:
  # |-------------|-------------|-------------|-------------|
  # |             |             |             |             |
  # |             |             |             |             |
  # |             |             |             |             |
  # | H H H H H H | H H H H H H | H H H H H H | H H H H H H |
  # |-------------|-------------|-------------|-------------|
  # | H H H H H H | H H H H H H | H H H H H H | H H H H H H |
  # |             |             |             |             |
  # |             |             |             |             |
  # | H H H H H H | H H H H H H | H H H H H H | H H H H H H |
  # |-------------|-------------|-------------|-------------|
  # | H H H H H H | H H H H H H | H H H H H H | H H H H H H |
  # |             |             |             |             |
  # |             |             |             |             |
  # | H H H H H H | H H H H H H | H H H H H H | H H H H H H |
  # |-------------|-------------|-------------|-------------|
  # | H H H H H H | H H H H H H | H H H H H H | H H H H H H |
  # |             |             |             |             |
  # |             |             |             |             |
  # |             |             |             |             |
  # |-------------|-------------|-------------|-------------|
  def __call__(self, block_plane):
    pixels        = block_plane.pixels
    block_width   = block_plane.block_width
    block_height  = block_plane.block_height
    pixels_width  = block_plane.pixel_plane_width
    pixels_height = block_plane.pixel_plane_height
    
    transformed   = block_plane.dup()
    transformed.pixels *= 0

    for y_block in range(2,block_plane.height()):
      y1 = y_block * block_height - 1
      y2 = y1 + 1
      for x in range(1,pixels_width):
        diff                     = abs(pixels[y1,x] - pixels[y2,x])
        transformed.pixels[y1,x] = diff
        transformed.pixels[y2,x] = diff    

    return transformed