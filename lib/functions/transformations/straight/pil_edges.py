import numpy as np
from PIL import Image, ImageFilter

class PilEdges:
  
  def __call__(self, block_plane):
    transformed = block_plane.dup()
    image       = Image.fromarray(transformed.pixels)

    image       = image.convert('L')
    image       = image.filter(ImageFilter.FIND_EDGES)

    transformed.pixels = np.array(image)
    return transformed