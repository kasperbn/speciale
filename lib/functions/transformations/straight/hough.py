import numpy as np

class Hough:
  
  def __init__(self, block_plane, thetas=[0, 180]):    
    self.thetas        = thetas
  
  def __call__(self, block_plane):
    pixels           = block_plane.pixels
    block_width      = block_plane.block_width
    block_height     = block_plane.block_height
    width            = block_plane.pixel_plane_width
    height           = block_plane.pixel_plane_height    
    
    max_rho          = sqrt(width**2 + height**2)
    number_of_thetas = len(self.thetas)
    votes            = np.zeros(shape=(number_of_thetas, max_rho))
  
    for t in range(0, number_of_thetas):
      for x in range(1,width):
        for y in range(1,height):
          theta         = self.thetas[t]
          pixel         = pixels[x,y]
          rho           = x*cos(theta)+y*sin(theta)
          votes[t,rho] += pixel

    return votes