from distance_to_neighbors import *

class SSDiffToNeighborMeans(DistanceToNeighbors):

  def distance(self, block, neighbor):
    return (block.mean() - neighbor.mean())**2
