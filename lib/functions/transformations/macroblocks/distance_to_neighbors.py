import numpy as np
from lib.functions.map import *
from lib.functions.block_neighbors import *

class DistanceToNeighbors:

  def __call__(self, block_plane):
    block_values    = self.block_values(block_plane)
    new_block_plane = block_plane.dup()
    for b in new_block_plane:
      new_block_plane[b.index] = block_plane.one_block() * block_values[b.index]
    return new_block_plane

  def block_values(self, block_plane):
    return  Map(
              self.map_neighbor_distances,
              self.reduce_neighbor_distances
            )(block_plane)

  def map_neighbor_distances(self, block):
    return [self.distance(block, n) for n in self.neighbors(block)]

  def reduce_neighbor_distances(self, distances):
    return sum(distances)

  def neighbors(self, block):
    return BlockEightNeighbors(block)()
    
  def distance(self, block, neighbor):
    raise NotImplementedError