from distance_to_neighbors import *
from lib.models.direction import *

class AbsDiffToNeighborBorders(DistanceToNeighbors):

  def neighbors(self, block):
    return BlockFourNeighbors(block)()

  def distance(self, block, neighbor):
    block_direction    = Direction.between(block.coordinate(), neighbor.coordinate())
    neighbor_direction = block_direction.reflection()
    return np.abs(block.border(block_direction) - neighbor.border(neighbor_direction)).sum()