class Grid:
  
  def __call__(self, block_plane):
    block_plane     = block_plane

    block_width     = block_plane.block_width
    block_height    = block_plane.block_height
    pixels_width    = block_plane.pixel_plane_width
    pixels_height   = block_plane.pixel_plane_height

    extra_col       = 0 if pixels_width  % block_width  == 0 else 1
    extra_row       = 0 if pixels_height % block_height == 0 else 1
    
    cols            = range(0, extra_col + pixels_width /block_width)
    rows            = range(0, extra_row + pixels_height/block_height)

    new_block_plane = block_plane.dup()
    
    for col in cols:
      new_block_plane.pixels[:,col*block_width] = 0
    
    for row in rows:
      new_block_plane.pixels[row*block_height,:] = 0
    
    return new_block_plane