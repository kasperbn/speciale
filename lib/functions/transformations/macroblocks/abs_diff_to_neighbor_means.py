from distance_to_neighbors import *

class AbsDiffToNeighborMeans(DistanceToNeighbors):
      
  def distance(self, block, neighbor):
    return abs(block.mean() - neighbor.mean())