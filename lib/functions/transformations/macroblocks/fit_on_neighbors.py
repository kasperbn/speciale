from distance_to_neighbors import *

class FitOnNeighbors(DistanceToNeighbors):
  
  def distance(self, block, neighbor):    
    mean      = neighbor.mean()
    variance  = neighbor.variance()
    
    p = (1 / np.sqrt(2*np.pi*variance)) * np.exp(-np.power(block.pixels-mean,2)/(2*variance))

    return -np.log(p).sum()