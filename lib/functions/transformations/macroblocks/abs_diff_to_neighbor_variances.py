from distance_to_neighbors import *

class AbsDiffToNeighborVariances(DistanceToNeighbors):
  
  def distance(self, block, neighbor):
    return abs(block.variance() - neighbor.variance())