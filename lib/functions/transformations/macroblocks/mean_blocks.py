class MeanBlocks:
  
  def __call__(self, block_plane):
    new_block_plane = block_plane.dup()

    for b in new_block_plane:
      new_block_plane[b.index] = block_plane.one_block() * b.mean()
          
    return new_block_plane