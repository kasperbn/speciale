from abs_diff_to_neighbor_means import *
from lib.functions.pipe import *

class AbsDiffOfAbsDiffToNeighborMeans:
        
  def __call__(self, block_plane):
    return Pipe(
      AbsDiffToNeighborMeans(),
      AbsDiffToNeighborMeans()
    )(block_plane)