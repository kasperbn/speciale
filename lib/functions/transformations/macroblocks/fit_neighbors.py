from distance_to_neighbors import *

class FitNeighbors(DistanceToNeighbors):

  def distance(self, block, neighbor):    
    mean      = block.mean()
    variance  = block.variance()
    
    p_neighbor = (1 / np.sqrt(2*np.pi*variance)) * np.exp(-np.power(neighbor.pixels-mean,2)/(2*variance))
    
    return -np.log(p_neighbor).sum()