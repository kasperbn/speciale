import numpy as np
from vendor.sp import ssim

def SSIM(planes):
  block_plane, prev_block_plane = planes

  x = block_plane.pixels
  y = prev_block_plane.pixels

  index = ssim.msssim(x,y)

  print "SSIM: %s"%index

  return index