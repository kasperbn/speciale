class Flatten:
  def __call__(self, array):
    flattened = []
    for x in array:
      if x.__class__ == list:
        flattened = flattened + Flatten()(x)
      else:
        flattened.append(x)
    return flattened