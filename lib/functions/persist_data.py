from persist_image import CreateDirUnlessExists
import pickle
import os

class PersistData:
  def __init__(self, path):
    self.path = path

  def __call__(self, data):
    CreateDirUnlessExists()(self.path)
    with open(self.path, 'w') as file:
      pickle.dump(data, file)
    return data

class LoadData:
  
  def __call__(self, path):
    with open(path, 'r') as file:
      return pickle.load(file)  