import os
import json
import subprocess
from movie import *
from yuv_movie import *

# Value object for a raw ProRes 422 movie
class ProResMovie(Movie):
    
  def __init__(self, file_path, **keywords):
    self.file_path  = file_path
    self.meta_data  = None
    self.width      = None
    self.height     = None
    self.frame_rate = None
    self.time_base  = None
    
    self.name       = self.file_path.split('/')[-1].split('.')[0]
    
    self.load_meta_data()
    self.block_size = keywords.pop('block_size', self.width/120)
  
  def load_meta_data(self):
    command        = "/home/vagrant/bin/ffprobe -v quiet -print_format json -show_format -show_streams -select_streams v %s" % self.file_path
    raw             = os.popen(command).read()
    self.meta_data  = json.loads(raw)

    video           = self.meta_data['streams'][0]
    
    self.width      = int(video['width'])
    self.height     = int(video['height'])
    self.frame_rate = int(video['r_frame_rate'].split('/')[0])
    self.time_base  = int(video['time_base'].split('/')[1])
      
  def __getitem__(self, frame_number):
    return self.yuv_movie()[frame_number]

  def __iter__(self):
    return ProResMovieIterator(self) 

  def frame_numbers(self):
    return self.yuv_movie().frame_numbers()
  
  def yuv_movie(self):
    source      = self.file_path
    destination = source.replace('.mov', '.yuv')
    if not os.path.exists(destination):
      os.popen("ffmpeg -i %s -pix_fmt yuv422p %s" % (source, destination))
    return YUVMovie(destination, self.width, self.height)

class ProResMovieIterator():
  
  def __init__(self, movie):
    self.movie         = movie
    self.current_index = 0
  
  def next(self):
    try:
      self.current_index += 1
      return self.movie[self.current_index]
    except:
      raise StopIteration