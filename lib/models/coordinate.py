class Coordinate:
  
  def __init__(self,x,y):
    self.x = x
    self.y = y
  
  def tuple(self):
    return (self.x, self.y)
  
  def __str__(self):
    return "(%s, %s)" % (self.x, self.y)