class Direction(int):
  
  NORTH_WEST = 0
  NORTH      = 1
  NORTH_EAST = 2  
  WEST       = 3
  CENTER     = 4
  EAST       = 5
  SOUTH_WEST = 6
  SOUTH      = 7
  SOUTH_EAST = 8
  
  @classmethod
  def between(cls, start, target):
    (xs, ys) = start.tuple()
    (xt, yt) = target.tuple()
    
    horizontal = xs - xt
    vertical   = ys - yt
        
    if vertical == 1: 
      value = cls.NORTH - horizontal
    elif vertical == 0:
      value = cls.CENTER - horizontal
    else:
      value = cls.SOUTH - horizontal
    
    return Direction(value)
  
  def reflection(self):
    if   self.is_north(): 
      return Direction(self.SOUTH)
    elif self.is_west(): 
      return Direction(self.EAST)
    elif self.is_east(): 
      return Direction(self.WEST)
    elif self.is_south(): 
      return Direction(self.NORTH)
    else:
      raise ValueError("%s has no reflection" % self)
  
  def is_north(self):
    return self == self.NORTH

  def is_west(self):
    return self == self.WEST

  def is_east(self):
    return self == self.EAST

  def is_south(self):
    return self == self.SOUTH
  
  def __str__(self):
    if   self == self.NORTH_WEST:
      return "NORTH_WEST"
    elif self == self.NORTH:
      return "NORTH"
    elif self == self.NORTH_EAST:
      return "NORTH_EAST"
    elif self == self.WEST:
      return "WEST"
    elif self == self.CENTER:
      return "CENTER"
    elif self == self.EAST:
      return "EAST"
    elif self == self.SOUTH_WEST:
      return "SOUTH_EAST"
    elif self == self.SOUTH:
      return "SOUTH"
    elif self == self.SOUTH_EAST:
      return "SOUTH_EAST"
