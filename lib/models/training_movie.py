class TrainingMovie:

  def __init__(self, movie_path, targets, scene_changes, frame_offset=0):
    self.movie_path    = movie_path
    self.targets       = targets
    self.scene_changes = scene_changes
    self.frame_offset  = frame_offset