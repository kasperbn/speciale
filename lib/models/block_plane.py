from block import *
from coordinate import *
from math import *
from lib.models.pixel_plane import *

# A block plane indexes macroblocks by coordinates (x,y) of by array index from top left to right bottom:
#
# By Coordinate:
# |-----|-----|-----|
# | 0,0 | 1,0 | 2,0 |
# |-----|-----|-----|
# | 0,1 | 1,1 | 2,1 |
# |-----|-----|-----|
# | 0,2 | 1,2 | 2,2 |
# |-----|-----|-----|
#
# By array index:
# |-----|-----|-----|
# |  0  |  1  |  2  |
# |-----|-----|-----|
# |  3  |  4  |  5  |
# |-----|-----|-----|
# |  6  |  7  |  8  |
# |-----|-----|-----|
class BlockPlane:
  
  def __init__(self, pixels, block_width, block_height, frame_number):
    self.pixels             = pixels
    self.block_width        = block_width
    self.block_height       = block_height
    self.frame_number       = frame_number

    self.pixel_plane_height = self.pixels.shape[0]
    self.pixel_plane_width  = self.pixels.shape[1]
        
  # Get block by index or coordinate:
  #   By coordinate: block_plane[Coordinate(2,1)]
  #   By index:      block_plane[7]
  def __getitem__(self, index_or_coordinate):
    index = self.index_or_coordinate_to_index(index_or_coordinate)

    row_start, row_end, col_start, col_end = self.index_to_pixel_ranges(index)
    block_pixels = self.pixels[row_start:row_end,col_start:col_end]
    return Block(block_pixels, self, index)

  # Set block by block or numpy array:
  #   By block:       block_plane[0] = block
  #   By numpy array: block_plane[0] = np.array([1,2,3,4])  
  def __setitem__(self, index_or_coordinate, value):
    index = self.index_or_coordinate_to_index(index_or_coordinate)
    if value.__class__ == Block:
      value = value.pixels
    row_start, row_end, col_start, col_end = self.index_to_pixel_ranges(index)
    self.pixels[row_start:row_end,col_start:col_end] = value
  
  def __len__(self):
    return self.resolution()
  
  def __iter__(self):
    return BlockPlaneIterator(self)  
  
  def index_to_pixel_ranges(self, i):
    col_start = (i % self.width()) * self.block_width 
    row_start = (i / self.width()) * self.block_height
    
    col_end = col_start + self.block_width
    row_end = row_start + self.block_height
    
    return row_start, row_end, col_start, col_end

  def resolution(self):
    return self.height() * self.width()

  def height(self):
    return self.pixel_plane_height / self.block_height
  
  def width(self):
    return self.pixel_plane_width / self.block_width
  
  def index_or_coordinate_to_index(self, index_or_coordinate):
    if index_or_coordinate.__class__ == Coordinate:
      return self.coordinate_to_index(index_or_coordinate)
    else:
      return index_or_coordinate
          
  def index_to_coordinate(self, index):
    x = index % self.width()
    y = index / self.width()
    return Coordinate(x, y)
  
  def coordinate_to_index(self, coordinate):
    x,y = coordinate.tuple()
    return y * self.width() + x

  def one_block(self):
    return np.ones([self.block_height, self.block_width])

  def dup(self):
    new_pixels    = np.empty_like(self.pixels)
    new_pixels[:] = self.pixels
    return BlockPlane(new_pixels, self.block_width, self.block_height, self.frame_number)
  
  def rot90(self):
    return BlockPlane(np.rot90(self.pixels), self.block_height, self.block_width, self.frame_number)
    
class BlockPlaneIterator():
  
  def __init__(self, block_plane):
    self.block_plane   = block_plane
    self.current_index = -1
    self.length        = len(self.block_plane)
  
  def next(self):
    if self.current_index < self.length-1:
      self.current_index += 1
      return self.block_plane[self.current_index]
    else:
      raise StopIteration