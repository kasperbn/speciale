import numpy as np

class LineSegment:
  
  def __init__(self, array):
    self.array  = np.array(array)

    self.length = self.array.size
    self.mean   = self.array.mean()
    self.var    = self.array.var()
