import numpy as np
from yuv_frame import *
from block_plane import *
from movie import *
import os

# Value object for a raw 4:2:2 yuv movie
class YUVMovie(Movie): 
    
  def __init__(self, file_path, width, height):
    self.file_path     = file_path
    self.width         = width
    self.height        = height
    self.block_size    = self.width/120
    
  def frame_numbers(self):
    file_size_bytes = os.stat(self.file_path).st_size

    y_bytes         = self.width * self.height
    u_bytes         = self.width/2 * self.height
    v_bytes         = self.width/2 * self.height

    return file_size_bytes / (y_bytes + u_bytes + v_bytes)

  # Extract the YUV components of a frame
  # 
  # Example: Extract the 100th frame
  # yuv_movie = YUVMovie('movie.yuv', 1920, 1080)
  # yuv_movie[100]
  def __getitem__(self, frame_number):
    resolution      = self.width * self.height
    half_resolution = resolution / 2
    half_width      = self.width / 2
    frame_offset    = resolution * (2 * (frame_number-1))
    
    with open(self.file_path, 'r') as f:
      f.seek(frame_offset)
      y = np.fromfile(f, np.uint8, resolution)
      u = np.fromfile(f, np.uint8, half_resolution)
      v = np.fromfile(f, np.uint8, half_resolution)
  
    y = np.reshape(y,(self.width, self.height),'F').transpose()
    u = np.reshape(u,(half_width, self.height),'F').transpose()
    v = np.reshape(v,(half_width, self.height),'F').transpose()
    
    # Convert to int32
    y = y.astype('int32')
    u = u.astype('int32')
    v = v.astype('int32')    
    
    y = BlockPlane(y, self.block_size, self.block_size, frame_number)
    u = BlockPlane(u, self.block_size/2, self.block_size, frame_number)
    v = BlockPlane(v, self.block_size/2, self.block_size, frame_number)
  
    return YUVFrame(frame_number,y,u,v)