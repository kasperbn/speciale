class Block:
  
  def __init__(self, pixels, block_plane, index=None):
    self.pixels      = pixels
    self.block_plane = block_plane
    self.index       = index
    
  def mean(self):
    return self.pixels.mean()
    
  def variance(self):
    return self.pixels.var()
    
  def shape(self):
    return self.pixels.shape
    
  def coordinate(self):
    return self.block_plane.index_to_coordinate(self.index)
  
  def border(self, direction):
    if direction.is_north():  
      return self.south_border()  
    elif direction.is_west():
      return self.west_border()
    elif direction.is_east():
      return self.east_border()
    elif direction.is_south():
      return self.north_border()
    else:
      raise LookupError("No border in %s" % direction)
  
  def north_border(self):
    return self.pixels[0,:]
  
  def west_border(self):
    return self.pixels[:,0]

  def east_border(self):
    return self.pixels[:,-1]

  def south_border(self):
    return self.pixels[-1,:]