class LazyTrainingPoint(TrainingPoint):
  
  @classmethod
  def init_from_movie_and_frame_number(cls, movie, frame_number, label):
    name = "%s_%s"%(movie.name(), frame_number)
    return TrainingPoint(name, movie, frame_number, label)
  
  def __init__(self, name, movie, frame_number, label):
    self.name         = name
    self.movie        = movie
    self.frame_number = frame_number
    self.label        = label
  
  def frame(self):
    return movie[frame_number]