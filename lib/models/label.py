# Value Object for label
class Label(int):

  NO_ERROR = 0
  ERROR    = 1
  
  @classmethod
  def no_error(cls):
    return Label(Label.NO_ERROR)
  
  @classmethod
  def error(cls):
    return Label(Label.ERROR)