from lib.functions import *
from lib.experiments import *
from lib.controllers import *
from config.training_movies import *

class LinesController(Controller):
  
  def all(self, movie_path, plane_name):
    return  self.lines_in_pil_edges(movie_path, plane_name) + \
            self.block_border_lines_in_pil_edges(movie_path, plane_name) + \
            self.block_border_lines_in_abs_diff_of_block_borders(movie_path, plane_name)
  
  def lines_in_pil_edges(self, movie_path, plane_name):
    return [LinesInPilEdgesExperiment(movie_path, plane_name, self.frame_start, self.frame_end)]

  def block_border_lines_in_pil_edges(self, movie_path, plane_name): 
    return [BlockBorderLinesInPilEdgesExperiment(movie_path, plane_name, self.frame_start, self.frame_end)]

  def block_border_lines_in_abs_diff_of_block_borders(self, movie_path, plane_name): 
    return [BlockBorderLinesInAbsDiffOfBlockBordersExperiment(movie_path, plane_name, self.frame_start, self.frame_end)]
