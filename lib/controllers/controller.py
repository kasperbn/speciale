import logging
from rq import Queue
from redis import Redis

class Controller:
  
  def __init__(self, experiments_name, movie_paths, plane_names, **keywords):
    self.experiments_name = experiments_name
    self.movie_paths      = movie_paths
    self.plane_names      = plane_names

    self.frame_start      = keywords.pop('frame_start', 1)
    self.frame_end        = keywords.pop('frame_end', None)

    self.movie_paths_and_plane_names_combinations = []
    for m in movie_paths:
      for p in plane_names:
        self.movie_paths_and_plane_names_combinations.append([m,p])
    
    self.queue            = Queue(connection=Redis(), default_timeout=7*24*60*60)

  def run(self, **keywords):    
    for [movie_path, plane_name] in self.movie_paths_and_plane_names_combinations:
      for e in self.experiments(movie_path, plane_name):
        try:
          e()
        except Exception, ex:
          logging.exception('Experiment failed')

  def enqueue(self):
    for [movie_path, plane_name] in self.movie_paths_and_plane_names_combinations:
      for e in self.experiments(movie_path, plane_name):
        print "Enqueing %s-%s > %s"%(movie_path, plane_name, e.name)
        self.queue.enqueue(e.__call__)
  
  def experiments(self, movie_path, plane_name):
    return getattr(self, self.experiments_name)(movie_path, plane_name)