from lib.functions import *
from lib.experiments import *
from lib.controllers import *
from config.training_movies import *

class MacroblockController(Controller):
    
  def all(self, movie_path, plane_name):
    return  self.abs_diff_to_neighbor_means(movie_path, plane_name) + \
            self.abs_diff_to_neighbor_borders(movie_path, plane_name) + \
            self.abs_diff_of_abs_diff_to_neighbor_means(movie_path, plane_name) + \
            self.abs_diff_to_neighbor_variances(movie_path, plane_name) + \
            self.fit_neighbors(movie_path, plane_name) + \
            self.fit_on_neighbors(movie_path, plane_name) + \
            self.mean_blocks(movie_path, plane_name) + \
            self.ss_diff_to_neighbor_means(movie_path, plane_name) + \
            self.variance_blocks(movie_path, plane_name)
  
  def abs_diff_to_neighbor_means(self, movie_path, plane_name):
    return [AbsDiffToNeighborMeansExperiment(movie_path, plane_name, self.frame_start, self.frame_end)]

  def abs_diff_to_neighbor_borders(self, movie_path, plane_name):
    return [AbsDiffToNeighborBordersExperiment(movie_path, plane_name, self.frame_start, self.frame_end)]

  def abs_diff_of_abs_diff_to_neighbor_means(self, movie_path, plane_name):
    return [AbsDiffOfAbsDiffToNeighborMeansExperiment(movie_path, plane_name, self.frame_start, self.frame_end)]

  def abs_diff_to_neighbor_variances(self, movie_path, plane_name):
    return [AbsDiffToNeighborVariancesExperiment(movie_path, plane_name, self.frame_start, self.frame_end)]

  def fit_neighbors(self, movie_path, plane_name):
    return [FitNeighborsExperiment(movie_path, plane_name, self.frame_start, self.frame_end)]

  def fit_on_neighbors(self, movie_path, plane_name):
    return [FitOnNeighborsExperiment(movie_path, plane_name, self.frame_start, self.frame_end)]

  def mean_blocks(self, movie_path, plane_name):
    return [MeanBlocksExperiment(movie_path, plane_name, self.frame_start, self.frame_end)]

  def ss_diff_to_neighbor_means(self, movie_path, plane_name):
    return [SSDiffToNeighborMeansExperiment(movie_path, plane_name, self.frame_start, self.frame_end)]

  def variance_blocks(self, movie_path, plane_name):
    return [VarianceBlocksExperiment(movie_path, plane_name, self.frame_start, self.frame_end)]