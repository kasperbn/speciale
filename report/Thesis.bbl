\begin{thebibliography}{KCYK13}

\bibitem[DH72]{duda1972a}
Duda and Hart.
\newblock Use of the hough transformation to detect lines and curves in
  pictures.
\newblock {\em Communications of the ACM}, 15(1):11--15, 1972.

\bibitem[FLA11]{5c800a}
Søren Forchhammer, Huiying Li, and Jakob~Dahl Andersen.
\newblock No-reference analysis of decoded mpeg images for psnr estimation and
  post-processing.
\newblock {\em J. Visual Communication and Image Representation},
  22(4):313--324, 2011.

\bibitem[Gro92]{jpeg_yuv_rgb}
Joint Photographic~Experts Group.
\newblock {JPEG File Interchange Format}.
\newblock \url{http://www.jpeg.org/public/jfif.pdf}, 1992.
\newblock [Online; accessed October-2013].

\bibitem[HA04]{hodge2004a}
VJ~Hodge and J.~Austin.
\newblock A survey of outlier detection methodologies.
\newblock {\em ARTIFICIAL INTELLIGENCE REVIEW}, 22(2):85--126, 2004.

\bibitem[KCYK12]{6145429}
Suk-Ju Kang, Sung~In Cho, Sungjoo Yoo, and Young-Hwan Kim.
\newblock Scene change detection using multiple histograms for
  motion-compensated frame rate up-conversion.
\newblock {\em Display Technology, Journal of}, 8(3):121--126, 2012.

\bibitem[KCYK13]{6486916}
Suk-Ju Kang, Sung~In Cho, Sungjoo Yoo, and Young~Hwan Kim.
\newblock Multi-histogram based scene change detection for frame rate
  up-conversion.
\newblock In {\em Consumer Electronics (ICCE), 2013 IEEE International
  Conference on}, pages 332--333, 2013.

\bibitem[KGV83]{simanneal}
S.~Kirkpatrick, CD~Gelatt, and MP~Vecchi.
\newblock Optimization by simulated annealing.
\newblock {\em SCIENCE}, 220(4598):671--680, 1983.

\bibitem[Vla00]{850446}
T.~Vlachos.
\newblock Detection of blocking artifacts in compressed video.
\newblock {\em Electronics Letters}, 36(13):1106--1108, 2000.

\bibitem[WBE00]{899622}
Zhou Wang, A.C. Bovik, and B.L. Evan.
\newblock Blind measurement of blocking artifacts in images.
\newblock In {\em Image Processing, 2000. Proceedings. 2000 International
  Conference on}, volume~3, pages 981--984 vol.3, 2000.

\end{thebibliography}
