\select@language {english}
\contentsline {chapter}{Short contents}{i}{section*.1}
\contentsline {chapter}{Abstract}{iii}{chapter*.2}
\contentsline {chapter}{Resumé}{v}{chapter*.3}
\contentsline {chapter}{Preface}{vii}{chapter*.4}
\contentsline {chapter}{Acknowledgements}{ix}{chapter*.5}
\contentsline {chapter}{Contents}{xi}{section*.6}
\contentsline {chapter}{\chapternumberline {1}Introduction}{1}{chapter.1}
\contentsline {section}{\numberline {1.1}Motivation}{1}{section.1.1}
\contentsline {section}{\numberline {1.2}Data Set}{2}{section.1.2}
\contentsline {section}{\numberline {1.3}Solution Requirements}{5}{section.1.3}
\contentsline {section}{\numberline {1.4}Goal}{5}{section.1.4}
\contentsline {section}{\numberline {1.5}Report Overview}{6}{section.1.5}
\contentsline {chapter}{\chapternumberline {2}Technical Background}{7}{chapter.2}
\contentsline {section}{\numberline {2.1}Video Coding}{7}{section.2.1}
\contentsline {subsection}{Exploitable Properties}{7}{section*.7}
\contentsline {subsection}{RGB Color Space}{8}{section*.8}
\contentsline {subsection}{Y'CbCr Color Space}{9}{section*.9}
\contentsline {subsection}{Chroma Subsampling}{10}{section*.10}
\contentsline {subsection}{DCT, Quantization and Entropy Coding}{12}{section*.11}
\contentsline {subsection}{ProRes Codec}{12}{section*.12}
\contentsline {section}{\numberline {2.2}Related Work}{13}{section.2.2}
\contentsline {chapter}{\chapternumberline {3}Methodology}{15}{chapter.3}
\contentsline {subsection}{General Flow Of The Detection Algorithms}{15}{section*.13}
\contentsline {section}{\numberline {3.1}Software}{16}{section.3.1}
\contentsline {subsection}{Programming Style}{16}{section*.14}
\contentsline {subsection}{Core Models}{16}{section*.15}
\contentsline {subsection}{Pure Functions}{17}{section*.16}
\contentsline {subsection}{Composability}{19}{section*.17}
\contentsline {section}{\numberline {3.2}Macroblock Transformations}{20}{section.3.2}
\contentsline {subsection}{Mean Blocks Transformation}{21}{section*.18}
\contentsline {subsection}{Variance Blocks Transformation}{22}{section*.19}
\contentsline {subsection}{Distance To Neighbors Transformation}{23}{section*.20}
\contentsline {subsection}{Abs Diff To Neighbor Means Transformation}{24}{section*.21}
\contentsline {subsection}{Abs Diff Of Abs Diff To Neighbor Means Transformation}{25}{section*.22}
\contentsline {subsection}{Sum Of Squared Diff To Neighbor Means Transformation}{26}{section*.23}
\contentsline {subsection}{Abs Diff To Neighbor Variances Transformation}{27}{section*.24}
\contentsline {subsection}{Abs Diff To Neighbor Borders Transformation}{28}{section*.25}
\contentsline {subsection}{Fit Neighbors Transformation}{29}{section*.26}
\contentsline {subsection}{Fit On Neighbors Transformation}{30}{section*.27}
\contentsline {section}{\numberline {3.3}Macroblock Experiments}{31}{section.3.3}
\contentsline {section}{\numberline {3.4}Line Transformations}{32}{section.3.4}
\contentsline {subsection}{PIL Edges Transformation}{32}{section*.28}
\contentsline {subsection}{Abs Diff Of Block Borders Transform}{33}{section*.29}
\contentsline {subsection}{Line Segments Counter}{34}{section*.30}
\contentsline {subsection}{Block Border Line Segments Counter}{34}{section*.31}
\contentsline {subsection}{Hough Transform Discussion}{35}{section*.32}
\contentsline {section}{\numberline {3.5}Line Experiments}{35}{section.3.5}
\contentsline {section}{\numberline {3.6}Experiment Flow}{36}{section.3.6}
\contentsline {subsection}{First Order Of Change}{36}{section*.33}
\contentsline {subsection}{Scene Detection}{36}{section*.34}
\contentsline {subsection}{Plots}{37}{section*.35}
\contentsline {subsection}{Performance Measurement}{37}{section*.36}
\contentsline {subsection}{Outlier Detection}{38}{section*.37}
\contentsline {subsection}{Simulated Annealing}{39}{section*.38}
\contentsline {subsection}{Entire Experiment Flow}{40}{section*.39}
\contentsline {chapter}{\chapternumberline {4}Results}{41}{chapter.4}
\contentsline {section}{\numberline {4.1}Macroblock Experiments}{41}{section.4.1}
\contentsline {subsection}{Mean Blocks Results}{42}{section*.40}
\contentsline {subsection}{Variance Blocks Result}{44}{section*.41}
\contentsline {subsection}{Abs Diff To Neighbor Means Results}{46}{section*.42}
\contentsline {subsection}{Abs Diff Of Abs Diff To Neighbor Means Results}{48}{section*.43}
\contentsline {subsection}{Sum Of Squared Diff To Neighbor Means Results}{50}{section*.44}
\contentsline {subsection}{Abs Diff To Neighbor Variances Results}{52}{section*.45}
\contentsline {subsection}{Abs Diff To Neighbor Borders Results}{54}{section*.46}
\contentsline {subsection}{Fit Neighbors Results}{56}{section*.47}
\contentsline {subsection}{Fit On Neighbors Results}{58}{section*.48}
\contentsline {section}{\numberline {4.2}Line Experiments}{59}{section.4.2}
\contentsline {subsection}{Lines In PIL Edges Results}{60}{section*.49}
\contentsline {subsection}{Block Border Lines in PIL Edges Results}{62}{section*.50}
\contentsline {subsection}{Block Border Lines in Abs Diff Of Block Borders Results}{64}{section*.51}
\contentsline {section}{\numberline {4.3}Experiments Ranked By Costs}{66}{section.4.3}
\contentsline {chapter}{\chapternumberline {5}Conclusion}{67}{chapter.5}
\contentsline {section}{\numberline {5.1}Future Work}{67}{section.5.1}
\contentsline {appendix}{\chapternumberline {A}Software \& Result Data}{69}{appendix.A}
\contentsline {section}{\numberline {A.1}Software}{69}{section.A.1}
\contentsline {section}{\numberline {A.2}Software Installation}{69}{section.A.2}
\contentsline {section}{\numberline {A.3}Result Data}{69}{section.A.3}
\contentsline {appendix}{\chapternumberline {B}Frames With Noise Artifacts}{71}{appendix.B}
\contentsline {chapter}{Bibliography}{77}{section*.52}
