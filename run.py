#!/usr/bin/python
import sys; sys.path.append('/vagrant/')
from lib import *
from argparse import ArgumentParser, RawDescriptionHelpFormatter

routes = [
  # Macroblock
  'macroblock:all',
  'macroblock:abs_diff_of_abs_diff_to_neighbor_means',
  'macroblock:abs_diff_to_neighbor_borders',
  'macroblock:abs_diff_to_neighbor_means',
  'macroblock:abs_diff_to_neighbor_variances',
  'macroblock:fit_neighbors',
  'macroblock:fit_on_neighbors',
  'macroblock:mean_blocks',
  'macroblock:ss_diff_to_neighbor_means',
  'macroblock:variance_blocks',

  # Lines
  'lines:all',
  'lines:lines_in_pil_edges',
  'lines:block_border_lines_in_pil_edges',
  'lines:block_border_lines_in_abs_diff_of_block_borders',
]

examples = """Examples:

    # Run all macroblock experiments on all movies in all planes
    $ ./run.py -y -u -v macroblock:all /vagrant/data/*.mov
    
    # Enqueue a particular experiment on a movie in the y-plane in frames 1-10
    $ ./run.py -y -s=1 -e=10 -q lines:count_border_line_segments /vagrant/data/possession.mov

Available Commands:

\t%s
  
"""%('\n\t'.join(routes))

parser = ArgumentParser(epilog=examples, formatter_class=RawDescriptionHelpFormatter)
parser.add_argument("-q", "--enqueue", action='store_true', dest="enqueue", default=False,
                  help="enqueue experiments in Redis queue")
parser.add_argument("-y", "--y-plane", action='store_true', dest="y_plane", default=False,
                  help="run experiment on y-plane")
parser.add_argument("-u", "--u-plane", action='store_true', dest="u_plane", default=False,
                  help="run experiment on u-plane")
parser.add_argument("-v", "--v-plane", action='store_true', dest="v_plane", default=False,
                  help="run experiment on v-plane")
parser.add_argument("-s", "--frame_start", dest="frame_start", type=int, default='1',
                  help="the frame number to start from")
parser.add_argument("-e", "--frame_end", dest="frame_end", type=int, default=None,
                  help="the frame number to end at")
parser.add_argument("experiment",
                  help="the experiment to run")
parser.add_argument("movie_paths", metavar='movie_path', type=str, nargs='+',
                  help="path(s) to ProRes movie")

try:
  args = vars(parser.parse_args())
except Exception, error:
  print error
  print ''
  parser.print_help()
  exit()

if not args['experiment'] in routes:
  print 'No such experiment: %s'%args['experiment']
  exit()

for movie_path in args['movie_paths']:
  if not os.path.exists(movie_path):
    print 'No such file: %s'%movie_path
    exit()
    
plane_names = []
if args['y_plane']:
  plane_names.append('y')
if args['u_plane']:
  plane_names.append('u')
if args['v_plane']:
  plane_names.append('v')

if args['frame_end'] != None:
  args['frame_end'] += 1

controller_name, experiment = args['experiment'].split(':')
controller_class            = eval(controller_name.title()+'Controller')
controller                  = controller_class(experiment, 
                                                args['movie_paths'], 
                                                plane_names,
                                                frame_start=args['frame_start'],
                                                frame_end=args['frame_end'])
                              
if args['enqueue']:
  controller.enqueue()
else:
  controller.run()